package com.phuocbui.tikitest.data;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiService {

    @GET
    Observable<List<String>> getKeywords(@Url String url);
}
