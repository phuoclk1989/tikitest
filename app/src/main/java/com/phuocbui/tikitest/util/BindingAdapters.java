package com.phuocbui.tikitest.util;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.widget.ImageView;
import android.widget.TextView;

import com.phuocbui.tikitest.BaseRecyclerViewAdapter;
import com.phuocbui.tikitest.R;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.reactivex.functions.Function;

public class BindingAdapters {

    @BindingAdapter("tiki:delivery_location")
    public static void setDeliveryLocation(TextView textView, String location) {
        String deliveryLocation = textView.getContext().getString(R.string.main_delivery_location_title, location);
        textView.setText(deliveryLocation);
    }

    @BindingAdapter("android:tint")
    public static void setTintColor(ImageView view, int color) {
        view.setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    @BindingAdapter("tiki:adapter")
    public static <T extends BaseRecyclerViewAdapter> void setApdater(RecyclerView recyclerView, T adapter) {
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter("tiki:onRefresh")
    public static void setOnRefresh(SwipeRefreshLayout view, SwipeRefreshLayout.OnRefreshListener listener) {
        view.setOnRefreshListener(listener);
    }
}
