package com.phuocbui.tikitest.hotkey;

import android.graphics.Color;

import java.util.Random;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

public class ItemHotKeyViewModel {
    public ObservableInt backgroundColor = new ObservableInt(0);
    public ObservableField<String> hotkey = new ObservableField<>();

    public ItemHotKeyViewModel(String keyword) {
        hotkey.set(keyword);
        int color = Color.rgb(randomInt(0, 100), randomInt(50, 100), randomInt(0, 100));
        backgroundColor.set(color);
    }

    private int randomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }
}
