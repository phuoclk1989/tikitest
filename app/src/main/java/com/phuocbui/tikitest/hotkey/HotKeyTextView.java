package com.phuocbui.tikitest.hotkey;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import androidx.appcompat.widget.AppCompatTextView;

public class HotKeyTextView extends AppCompatTextView {


    public HotKeyTextView(Context context) {
        super(context);
    }

    public HotKeyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HotKeyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(formatText(text.toString()), type);
    }

    private String formatText(String text) {
        // standard input
        if (text == null) text = "";
        text = text.trim();
        String[] words = text.split(" ");

        // format with text has > 2 words
        if (words.length >= 2) {
            Log.d("HotKeyTextView", "keyword:" + text);

            // flag to check this text has formatted or not
            boolean isSeparated = false;

            // middle length of text (exclude space chars)
            float average = (text.length() - words.length + 1) / 2f;

            StringBuilder builder = new StringBuilder(words[0]);

            int currentLength = words[0].length();
            for (int i = 1; i < words.length; i++) {
                int nextLength = currentLength + words[i].length();
                float mid = (nextLength + currentLength) / 2f;
                Log.d("HotKeyTextView", "currentLength:" + currentLength + ", nextLength:" + nextLength + "average:" + average);
                if (!isSeparated && mid > average) {
                    Log.d("HotKeyTextView", "Separate! Append new line");
                    isSeparated = true;
                    builder.append("\n");
                    builder.append(words[i]);

                } else {
                    Log.d("HotKeyTextView", "Append word");
                    builder.append(" ");
                    builder.append(words[i]);
                    currentLength += words[i].length();
                }
            }
            text = builder.toString();
        }
        return text;
    }
}
