package com.phuocbui.tikitest.hotkey;

import com.phuocbui.tikitest.BaseRecyclerViewAdapter;
import com.phuocbui.tikitest.R;

public class HotKeyAdapter extends BaseRecyclerViewAdapter<ItemHotKeyViewModel> {
    @Override
    protected int getLayoutId(int viewType) {
        return R.layout.item_hotkey;
    }
}
