package com.phuocbui.tikitest;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.phuocbui.tikitest.data.DataManager;
import com.phuocbui.tikitest.hotkey.HotKeyAdapter;
import com.phuocbui.tikitest.hotkey.ItemHotKeyViewModel;

import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel {
    public ObservableField<String> deliveryLocation = new ObservableField<>();
    public HotKeyAdapter adapter;
    public MutableLiveData<Boolean> isRefreshing = new MutableLiveData<>();
    private DataManager dataManager;
    private CompositeDisposable disposeBag = new CompositeDisposable();

    public MainViewModel() {
        deliveryLocation.set("Phường Phú Mỹ, Quận 7, Hồ Chí Minh");
        adapter = new HotKeyAdapter();
        dataManager = new DataManager();
        isRefreshing.setValue(false);
        getData();
    }

    public void onDeliveryLocationClick(View view) {
        Toast.makeText(view.getContext(), "Click on delivery location.", Toast.LENGTH_SHORT).show();

    }

    public void onRefresh() {
        getData();
    }

    private void getData() {
        isRefreshing.setValue(true);
        adapter.itemsSource.clear();
        disposeBag.add(dataManager.getKeywords()
                .flatMap(Observable::fromIterable)
                .map(ItemHotKeyViewModel::new)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(item -> adapter.itemsSource.add(item),
                        e -> Log.e("MainViewModel", "Cannot get data: " + e),
                        () -> isRefreshing.setValue(false)));
    }

    public void onCleared() {
        disposeBag.dispose();
    }
}
